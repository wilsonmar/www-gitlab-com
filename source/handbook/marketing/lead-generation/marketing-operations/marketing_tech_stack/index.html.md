---
layout: markdown_page
title: “Marketing Operations“
---

## Tech Stack Details

### [Tools](#tools)
- [Clearbit](#clearbit)
- DiscoverOrg
- FunnelCake
- Google Analytics
- Google Tag Manager
- [InsideView](#insideview)
- [Marketo](#marketo)
- On24
- Outreach
- Piwik
- Salesforce
- Unbounce

## Clearbit<a name="clearbit"></a>  
[Executed contract copy](https://drive.google.com/open?id=0BzllC63GKDQHMTk2VnE5eWx3NHFTNjBwdHdmWXNCbG10d3pn) (can only view if GitLab Team Member &amp; signed into your account)  
Effective Date: 22 March 2017  
End Date: 21 March 2018 (12-month term)  
Cancellation Clause: 30-day notice to non-renew  
**GitLab Admins**: JJ Cordz or Francis Aquino  
<br>
Details:   
- 500.000 records: database enrichment size can be increased in batches of 100k  
- 35 user licenses: as of 11 April, 33 licenses used. Additional licenses can be purchased in groups of 5 users.   

### Frequently Asked Questions

1. **What standard fields does Clearbit write to?** Clearbit will write to any standard field that is either blank or has a value of '[[unknown]]' if there is data to populate.

    **Lead**  
    First Name  
    Last Name (if value is [[unknown]])  
    Company (if value is [[unknown]])  
    Title  
    Phone (Company Line only)  
    Website  
    Address  
    No. of Employees  
    Industry  
    Description  
    Annual Revenue (for public companies)  

    **Contact**  
    First Name  
    Last Name (if value is [[unknown]])  
    Title  
    Mailing Address  
    Phone  
    Description  

    **Account**  
    Account Name (if value is [[unknown]])  
    Billing Address  
    Phone  
    Industry  
    Employees  
    Description  
    Annual Revenue (for public companies)  

2. **What fields does Clearbit use to identify people and companies?** The integration looks at the _email_ and _website_ fields on **leads** and **contacts**, and the _website_ field on **accounts**.

3. **Does Clearbit data update automatically?** YES - Clearbit data is automatically researched whenever a lead/contact/account is created, even if the record is not manaully opeened by a user. This happens through a Salesforce background job. The lead will also be triggred when the lead is viewed. 

## InsideView<a name="insideview"></a>  
[Executed Contract copy](https://drive.google.com/open?id=0BzllC63GKDQHRWJLaFhiV3VOVmlUOXBLQmhmTnJrcjVRMVo4)  (can only view if GitLab Team Member &amp; signed into your account)  
Effective Date: 1 October 2016  
End Date: 30 September 2017 (12-month term)  
Cancellation Clause: 30-day notice to non-renew _Notice of non-renew given on 22 May 2017_  
**GitLab Admin**: JJ Cordz  


## Marketo<a name="marketo"></a>  
Effective Date: 1 October 2016  
End Date: 30 September 2017  
**GitLab Admins**: JJ Cordz or Mitchell Wright  