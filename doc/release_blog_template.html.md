---
release_number: "x.x"
title: "GitLab x.x Released with XXX and XXX"
author: Name Surname
author_gitlab: GitLab.com-username
author_twitter: Twitter-username
image_title: '/images/x_x/x_x-cover-image.jpg'
description: "GitLab x.x Released with Main-CE-Feature, Main-EE-Feature, Secondary-Feature, Secondary-Feature, and much more!"
twitter_image: '/images/tweets/gitlab-x-x-released.jpg'
categories: release
# extra_css:
#  - release-posts.css
# extra_js:
#  - release-posts.js
---

<!--
- Add the content here. _Ad hoc_ free until the 6th working day before the 22nd. After that, we'll freeze the post.
- Content review: will take place during the 5th and the 4th working day before the 22nd.
- Styles will be applied in the 3rd to 1st working day before the 22nd. How to apply styles: check the release post handbook: https://about.gitlab.com/handbook/marketing/blog/relese-post/.

**Use the merge request template "Release-Post", and please set the calendar date for each stage.**

-->

## Introduction

Enter the introduction here

## MVP

<!--

- Name and Surname:
- GitLab.com handle: https://gitlab.com/username
- Contributed to:
- Merge request link:
- Short description (example below):

-->

Example <!-- delete this example when you add the MVP data -->

"Jacopo made it possible to [undo marking a todo item as done in the todos list](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/8691). This is a huge productivity enhancement that helps you recover from mistakes in managing todos. Thanks Jacopo!" 

<!--
## Cover image:

Link to original image:
Image name (if available):
Author's name (if available):
Licensed under:
Link to license:
-->

## Upgrade barometer

- DESCRIBE HOW INVOLVED THE MIGRATIONS ARE. CAN USERS EXPECT MUCH DOWNTIME?
- CHECK IF THERE ARE ANY MIGRATIONS THAT REMOVE OR CHANGE COLUMNS.
- IF THERE ARE ONLY ADDITIONS OR NO MIGRATIONS CONFIRM THAT DEPLOY CAN BE WITHOUT DOWNTIME

## Performance improvements

Describe it as a feature.

## Omnibus improvements

- Describe all improvements here. Link to docs, issues, and MRs whenever applicable.

## Extras

Are there any extra stuff to be added, or noted? Please describe them here. E.g.: events, news, notifications, etc.

## Deprecations

Anything being deprecated? Please fill the following block. Copy and paste the items as much as necessary.
 
<!--
Template

### GitLab XXX Deprecation

- Feature name:
- Available in: (CE/EES/EEP)
- Link to issue or MR:

Description:

Due date:
-->

----

<!-- FEATURES -->

## FEATURES 🚀

<!--
For each feature added to the post, provide the required info. Use dashes to separate them. Push all related images to the MR and provide the correct link. Compress every image with [Tiny PNG](https://tinypng.com/) or similar tool.
-->

<!-- Template:
## Feature name


- Available in: (CE/EES/EEP)
- Feature weight: primary, secondary
- Documentation link:
- Feature description, related images, and videos:

----

Example:

## Deploy Boards

- Available in: EEP
- Feature weight: primary
- Documentation link: https://docs.gitlab.com/ee/user/project/deploy_boards.html
- Feature description, related images, and videos:

Today with 9.0, we are excited to release Deploy Boards for environments running on Kubernetes. The Environments page of Pipelines now offers a single place to view the current health and deployment status of each environment, displaying the specific status of each pod in the deployment. Developers and other teammates can view the progress and status of a rollout, pod by pod, in the workflow they already use without any need to access Kubernetes.

![Deploy Boards](/images/9_0/deploy_boards.png){: .shadow}

Watch the video:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/enMumwvLAug" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

----
-->

