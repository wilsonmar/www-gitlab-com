module Gitlab
  module Homepage
    class Team
      class Project
        attr_reader :key, :assignments

        def initialize(key, data)
          @key = key
          @data = data
          @assignments = []
        end

        def name
          @data.fetch('name')
        end

        def description
          @data.fetch('description')
        end

        def link
          @data.fetch('link')
        end

        def path
          @data.fetch('path')
        end

        def mirrors
          @data.fetch('mirrors', [])
        end

        def assign(member)
          member.roles[key].each do |role|
            @assignments << Team::Assignment.new(member, self, role)
          end
        end

        def owners
          @assignments.select(&:owner?)
        end

        def maintainers
          @assignments.select(&:maintainer?)
        end

        def reviewers
          @assignments.select(&:reviewer?)
        end

        def self.all!
          @projects ||= YAML.load_file('data/projects.yml')

          @projects.map do |key, data|
            self.new(key, data).tap { |project| yield project if block_given? }
          end
        end
      end
    end
  end
end
